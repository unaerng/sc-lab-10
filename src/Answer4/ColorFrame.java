package Answer4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel colorPanel;
	private JComboBox colorCombo;
	private ChangeListener listener;

	private static final int WIDTH = 400;
	private static final int HEIGHT = 500;

	public ColorFrame() {
		
		class ColorListener implements ChangeListener
		{
			@Override
			public void stateChanged(ChangeEvent event) {
				setSampleColor();

			}
		}

		listener = new ColorListener(); 
		
		colorPanel = new JPanel();

		add(colorPanel ,BorderLayout.CENTER);
		createColorPanel();
		setSampleColor();
		setSize(WIDTH,HEIGHT);
	}

	public JPanel createColorPanel(){

		colorCombo = new JComboBox<>();
		colorCombo.addItem("RED");
		colorCombo.addItem("GREEN");
		colorCombo.addItem("BLUE");
		colorCombo.setEditable(false);
		colorCombo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (colorCombo.getSelectedItem()=="RED") {
					
					colorPanel.setBackground(Color.RED);
				}
				else if (colorCombo.getSelectedItem()=="GREEN") {
					
					colorPanel.setBackground(Color.GREEN);
				}
				else if (colorCombo.getSelectedItem()=="BLUE") {
					
					colorPanel.setBackground(Color.BLUE);
				}
				colorPanel.revalidate();
				colorPanel.repaint();
			}
		});

		JPanel controlPanel = new JPanel();

		controlPanel.add(colorCombo);

		add(controlPanel,BorderLayout.SOUTH);
		return controlPanel;

	}

	public void setSampleColor(){
		if (colorCombo.getSelectedItem()=="RED") {
			
			colorPanel.setBackground(Color.RED);
		}
		else if (colorCombo.getSelectedItem()=="GREEN") {
			
			colorPanel.setBackground(Color.GREEN);
		}
		else if (colorCombo.getSelectedItem()=="BLUE") {
			
			colorPanel.setBackground(Color.BLUE);
		}
		colorPanel.revalidate();
		colorPanel.repaint();
	} 
}

