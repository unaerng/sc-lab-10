package Answer5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	public JMenu menu;
	public JMenuBar menubar;
	public JMenuItem menuRedItem,menuGreenItem,menuBlueItem;
	private JPanel colorPanel;
	private JSlider redSlider;
	private JSlider greenSlider;
	private JSlider blueSlider;

	private static final int WIDTH = 400;
	private static final int HEIGHT = 500;

	public ColorFrame() {
		colorPanel = new JPanel();
		menubar = new JMenuBar();
		setJMenuBar(menubar);

		add(colorPanel ,BorderLayout.CENTER);

		setSize(WIDTH,HEIGHT);

		menu = new JMenu("Color");
		menubar.add(menu);

		menuRedItem = new JMenuItem("Red");
		menu.add(menuRedItem);
		menuRedItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				colorPanel.setBackground(Color.RED);
			}
		});

		menuGreenItem = new JMenuItem("Green");
		menu.add(menuGreenItem);
		menuGreenItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				colorPanel.setBackground(Color.GREEN);
			}
		});

		menuBlueItem = new JMenuItem("Blue");
		menu.add(menuBlueItem);
		menuBlueItem.addActionListener((new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				colorPanel.setBackground(Color.BLUE);
			}
		}));

	}
}
